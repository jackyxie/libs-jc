<?php
namespace Jclibs;

class Http {

    public static function curlGet ($url, $headers = [], $proxy = false, $resolve = [], $timeout = 10, $http_version = CURL_HTTP_VERSION_NONE) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        if(!empty($headers) && is_array($headers)){
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }

        if ($proxy) {
            curl_setopt($ch, CURLOPT_PROXYTYPE, CURLPROXY_SOCKS5_HOSTNAME);
            curl_setopt($ch, CURLOPT_PROXY, $proxy['ip'] . ':' . $proxy['port']);
        }

        //指定解析地址
        if (!empty($resolve) && is_array($resolve)) {
            curl_setopt($ch, CURLOPT_RESOLVE, $resolve);
        }

        //debug
//        curl_setopt($ch, CURLOPT_VERBOSE, true);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
        curl_setopt($ch, CURLOPT_ENCODING, '');
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_AUTOREFERER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, $http_version);

        //dns cache timeout
//        curl_setopt($ch, CURLOPT_DNS_CACHE_TIMEOUT, 5);
        $output = curl_exec($ch);
        $result['code'] = curl_getinfo($ch,CURLINFO_HTTP_CODE);
        curl_close($ch);

        $tp = json_decode($output, true);
        if (empty($tp) || !is_array($tp)) {
            $result['content'] = $output;
        }else {
            $result['content'] = $tp;
        }

        return $result;
    }


    public static function curlPost ($url, $postData = [], $headers = [], $queryStr = '', $proxy = false, $timeout = 10) {

        if(!empty($queryStr)){
            $url .= '?'. $queryStr;
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);

        if(!empty($headers) && is_array($headers)){
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
        curl_setopt($ch, CURLOPT_AUTOREFERER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);


        if ($proxy) {
            curl_setopt($ch, CURLOPT_PROXYTYPE, CURLPROXY_SOCKS5_HOSTNAME);
            curl_setopt($ch, CURLOPT_PROXY, $proxy['ip'] . ':' . $proxy['port']);
        }

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);

        $output = curl_exec($ch);
        $result['code'] = curl_getinfo($ch,CURLINFO_HTTP_CODE);
        curl_close($ch);

        $tp = json_decode($output, true);
        if (empty($tp) || !is_array($tp)) {
            $result['content'] = $output;
        }else {
            $result['content'] = $tp;
        }

        return $result;
    }
}