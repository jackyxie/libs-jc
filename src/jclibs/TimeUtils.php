<?php
namespace Jclibs;

class TimeUtils {
    /**
     * 获取毫秒时间戳
     *
     * @return int
     */
    public static function getCurrentMilliTimestamp () {
        return intval(microtime(true) * 1000);
    }

    /**
     * 获取秒时间戳
     *
     * @return int
     */
    public static function getCurrentSecTimestamp () {
        return intval(time());
    }

    /**
     * 获取当前时间的UTC时间字符串
     * eg: 2019-06-13T03:53:53.846352Z
     *
     * @param bool $end_digit 小数点位数, true: 3位小数(毫秒), false: 6位小数(微秒)
     * @return string
     */
    public static function getCurrentUtcTimeStr ($end_digit = false) {
        $dateTime = new \DateTime('now', new \DateTimeZone('UTC'));
        $utc_str = $dateTime->format('Y-m-d\TH:i:s.u');
        if ($end_digit) {
            $utc_str = substr($utc_str, 0, -3);
        }
        $utc_str .= 'Z';
        return $utc_str;
    }

    /**
     * 日期转换为UTC日期字符串
     *
     * @param string $date_time_str 日期时间字符串,符合strtotime参数的数据
     * 如: 2019-01-13 12:01:24
     * @param int $decimal //秒后面的小数位数
     * @param bool $endfix //是否需要Z后缀
     * @return string 2019-01-13T04:01:24.000000Z
     */
    public static function timeStrToUtc ($date_time_str, $decimal = 6, $endfix = true) {
        $timestamp = strtotime($date_time_str);
        $datetime = new DateTime('@'. $timestamp, new DateTimeZone('UTC'));

        if ($decimal != 0) {
            $utc_time_str = $datetime->format('Y-m-d\TH:i:s.u');
            if ($decimal < 6) {
                $utc_time_str = substr($utc_time_str, 0, -$decimal);
            }
        }else {
            $utc_time_str = $datetime->format('Y-m-d\TH:i:s');
        }

        if ($endfix) {
            $utc_time_str .= 'Z';
        }

        return $utc_time_str;
    }

    /**
     * UTC 格式时间字符串转换为毫秒时间戳
     * @param string $utc_time  //时间字符串,如:2019-05-06T10:41:35.090Z | 2019-05-06T10:41:35.09
     * @return int
     */
    public static function utcToMicrotime ($utc_time) {
        //毫秒3位
        $micro = '000';
        $time_tmp = explode('.', $utc_time);
        if (count($time_tmp) === 2) {
            $micro = str_pad(strval(str_replace('Z', '', $time_tmp[1])), 3, '0', STR_PAD_RIGHT);
        }
        $micro = substr($micro, 0, 3);

        //时间戳,与时区无关
        $mstime = intval(strtotime($utc_time) . $micro);
        return $mstime;
    }
}